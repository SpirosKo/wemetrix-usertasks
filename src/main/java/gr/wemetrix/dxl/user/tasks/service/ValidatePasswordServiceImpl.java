package gr.wemetrix.dxl.user.tasks.service;


import gr.wemetrix.dxl.user.tasks.exceptions.UserErrorEnum;
import gr.wemetrix.dxl.user.tasks.exceptions.UserUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ValidatePasswordServiceImpl implements ValidatePasswordService {

  @Override
  public void validatePassword(String newPassword)  {

      if(!newPassword.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$"))
      {
        throw  new UserUtils()
            .createUserException(UserErrorEnum.INVALID_REQUEST.toString(), "Password not valid",
                HttpStatus.BAD_REQUEST);
      }

  }
}
