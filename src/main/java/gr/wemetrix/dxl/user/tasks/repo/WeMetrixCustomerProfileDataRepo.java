package gr.wemetrix.dxl.user.tasks.repo;


import gr.wemetrix.dxl.user.tasks.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WeMetrixCustomerProfileDataRepo extends MongoRepository<User, String> {
  User findByUsername(String username);

}

