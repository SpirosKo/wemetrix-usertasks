package gr.wemetrix.dxl.user.tasks.model;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "user")
public class User {

  @Id
  private String username;
  private String surname;
  private String name;
  private String bussinnessRole;
  private String role;
  private String rights;
  @Indexed(unique = true)
  private String email;
  private String password;
  private String mobile;
  private String workPhone;
  private Company company;

}
