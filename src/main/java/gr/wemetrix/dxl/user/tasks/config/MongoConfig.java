package gr.wemetrix.dxl.user.tasks.config;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

@Configuration
public class MongoConfig extends AbstractMongoConfiguration {

	@Value("${mongodb.host}")
	private String mongoHost;

	@Value("${mongodb.database}")
	private String mongoDB;

	@Value("${mongodb.document.ttl.inSeconds:604800}")
	private int ttlInSeconds;

	@Override
	protected String getDatabaseName() {
		return this.mongoDB;
	}

	@Override
	public MongoClient mongoClient() {

		MongoClientOptions.Builder optionsBuilder = MongoClientOptions.builder();
		optionsBuilder.connectTimeout(5000);
		optionsBuilder.socketTimeout(5000);
		optionsBuilder.serverSelectionTimeout(5000);

		MongoClientURI uri = new MongoClientURI(this.mongoHost, optionsBuilder);
		MongoClient mongoClient = new MongoClient(uri);
		return mongoClient;
	}

}