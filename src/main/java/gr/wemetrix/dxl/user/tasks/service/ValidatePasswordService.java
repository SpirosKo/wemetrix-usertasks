package gr.wemetrix.dxl.user.tasks.service;

public interface ValidatePasswordService {
  void validatePassword(String newPassword) throws Exception;
}
