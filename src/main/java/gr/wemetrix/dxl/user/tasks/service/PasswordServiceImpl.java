package gr.wemetrix.dxl.user.tasks.service;

import gr.wemetrix.dxl.user.tasks.model.User;
import gr.wemetrix.dxl.user.tasks.repo.WeMetrixCustomerProfileDataRepo;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService{

  @Autowired
  WeMetrixCustomerProfileDataRepo weMetrixCustomerProfileDataRepo;
  @Override
  public boolean updatePassword(String username, String password, String newPassword){

    User user = weMetrixCustomerProfileDataRepo.findByUsername(username);

    String s = Optional.ofNullable(user)
        .filter(Objects::nonNull)
        .map(item->item.getPassword()).get();
     Optional.ofNullable(user)
        .filter(Objects::nonNull)
        .filter(pwd->pwd.getPassword().equalsIgnoreCase(password))
        .map(updatedUser->{updatedUser.setPassword(newPassword);
          weMetrixCustomerProfileDataRepo.save(updatedUser);
        return true;})
        .orElse(false);
     return true;
  }

}
