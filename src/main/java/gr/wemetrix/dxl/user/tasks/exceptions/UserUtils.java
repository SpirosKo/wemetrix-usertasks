package gr.wemetrix.dxl.user.tasks.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class UserUtils {

  public static WeMetrixException createUserException(String error, String descr, HttpStatus code) {
    WeMetrixException weMetrixException = new WeMetrixException();
    weMetrixException.setError(error);
    weMetrixException.setError_description(descr);
    weMetrixException.setCode(code);
    return weMetrixException;

  }
}
