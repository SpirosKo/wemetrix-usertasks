package gr.wemetrix.dxl.user.tasks.exceptions;

import org.springframework.http.HttpStatus;

public class WeMetrixException extends RuntimeException {
  private String error_description;
  private String error;
  private HttpStatus code;

  public String getError_description() {
    return error_description;
  }

  public void setError_description(String error_description) {
    this.error_description = error_description;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public HttpStatus getCode() {
    return code;
  }

  public void setCode(HttpStatus code) {
    this.code = code;
  }

  public WeMetrixException() {
  }
}
