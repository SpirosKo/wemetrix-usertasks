package gr.wemetrix.dxl.user.tasks.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "company")
public class Company {

    @Id
    private String name;
    private String logo;
    private String url;
    private String vat;
    private String relationship;
    private String cfo;
    private String address;
    private String telephone;
    private String type;

  }




