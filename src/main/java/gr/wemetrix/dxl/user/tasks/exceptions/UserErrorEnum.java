package gr.wemetrix.dxl.user.tasks.exceptions;

public enum UserErrorEnum {
  INVALID_REQUEST("invalid_request");

  private String error;

  UserErrorEnum(String error) {
    this.error = error;
  }

}
