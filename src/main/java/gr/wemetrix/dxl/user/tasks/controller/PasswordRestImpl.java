package gr.wemetrix.dxl.user.tasks.controller;

import gr.wemetrix.dxl.user.tasks.service.PasswordService;
import gr.wemetrix.dxl.user.tasks.service.ValidatePasswordService;
import gr.wemetrix.dxl.user.tasks.service.ValidateService;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/updatePassword")
public class PasswordRestImpl {

  @Autowired
  PasswordService passwordService;
  @Autowired
  ValidateService validateService;
  @Autowired
  ValidatePasswordService validatePasswordService;


  @PostMapping
  public ResponseEntity<Boolean> updatePassword(
      @NotEmpty @NotNull @RequestParam(name = "user") String user,
      @NotEmpty @NotNull @RequestParam(name = "password") String pwd,
      @NotEmpty @NotNull @RequestParam(name = "newPassword") String newPwd
  ) throws Exception {
    validatePasswordService.validatePassword(newPwd);
     validateService.checkIfPasswordExists(pwd,newPwd);
    return ResponseEntity.ok().body(passwordService.updatePassword(user, pwd,newPwd));
  }

}
