package gr.wemetrix.dxl.user.tasks.service;

import gr.wemetrix.dxl.user.tasks.exceptions.UserErrorEnum;
import gr.wemetrix.dxl.user.tasks.exceptions.UserUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ValidateServiceImpl implements ValidateService {

  @Override
  public boolean checkIfPasswordExists(String password, String newPassword) throws Exception {
    if(password.equals(newPassword))
      throw // new Exception("Password already exists");
    new UserUtils()
        .createUserException(UserErrorEnum.INVALID_REQUEST.toString(), "Password already exists",
            HttpStatus.BAD_REQUEST);
    return true;
  }

}
