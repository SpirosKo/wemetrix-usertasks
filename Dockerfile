FROM adoptopenjdk:8-jdk-openj9-bionic
EXPOSE 8080
COPY target/weMretixCustomerProfileData-1.0.0.jar /app.jar

VOLUME /tmp

RUN addgroup vfgr
RUN adduser vfgrusr
RUN usermod -aG vfgr vfgrusr

USER vfgrusr

ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar --spring.config.location=/config/application.properties
